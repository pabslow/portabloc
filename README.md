# Portabloc CMS

A tiny CMS for creative coders.

Create a beautiful blog just from plain text files. Drag and drop them to your /posts folder. Portabloc does the ~hard work~ magic for you 🔮🪄

* No databases, but your files and folders.
* Supports both Markdown and Gemini syntax.
* Tagging support
* Easy. Flexible. Portable.
* Lightweight framework
* Less than < 300 KiB

## Demo
↗️ Live demo: [portabloc.xyz](https://portabloc.xyz)

![Portabloc](https://portabloc.xyz/assets/placeholder-2.png)

![Portabloc](https://portabloc.xyz/assets/portabloc.jpg)
