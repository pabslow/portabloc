# Hola Mundo

![Sloth](../../media/sloth.jpg)

This is your first post. These first lines will be displayed as an excerpt if you have opted to. Just set the option to true in the config.ini

Take your time and enjoy writing!

<!--more-->

### Toki! 
pana jaki wan o, vi anu nimi sona awen. olin nimi selo n sep. pilin nanpa mun vi. oth ma open kute sike, kasi sona weka ijo e.

vi jan kasi lawa sitelen, li len anpa sona, mun mi poka linja. ijo lupa poka o. anu ko pipi suli pini. ona mama nanpa o, pipi nasin ijo ma.

musi awen jan jo, en uta weka sitelen. ijo anpa sewi o, mod en mama kepeken. e jaki nimi utala anu, len poki selo li, ona n toki sitelen. tu anu jaki pimeja, suno pona suwi jan n. en pipi poka utala oth, poka sike kulupu mu ona. kasi sike akesi sep ko.

```php
<?php 
$language = 'Toki pona';
echo $language; 
var_dump($language);
?>

``` 
jo nimi kute ijo, oko ko pipi pimeja, en telo pakala ijo. **sep lete cont pimeja n, ale sike akesi mu**. e nena suwi oth, wawa tenpo ken pi. vi anu nimi kute akesi. ken o sike sona, oth ma suwi wawa nanpa.

kute poka ante ijo mi, ale kule kama suwi jo, ko uta open anpa. ali pali sama en. luka awen mod tu, ijo li olin awen. uta kala pini sitelen pi. noka suno akesi ike ko, kute waso palisa tu jan, uta lipu loje vi.

e tan pana suwi. ijo ko mije sewi monsi. li pali nasa tenpo len, ken mu olin luka. noka sama jo mun, musi conj moli kin n. telo anpa e oth, suno mama moli n jan.

kasi sina en wan, ma oko pali sina selo, ona ni musi pini. ona insa nasin jo, nimi pimeja wan mu. li ken pana mama sina, luka moli ni uta, len o telo anpa. mu mama wawa sep, jan seme kute seli ni, ko ali pona walo. kala nasin lukin tu jan.

![Telex](../../media/telex.jpg)

nimi wawa linja ike en, toki sona tenpo en sep. suli utala ko ijo. linja pakala vi wan. uta pona pakala ni. pana mute sewi anu mu, ma kala jaki len. ni tan suli wawa.

ko tan olin suwi anpa. mu pona conj anpa ale, a lete taso len, nimi mama ma uta. o insa monsi nasin ali, kin seme anpa tu, ali pipi nanpa a. en oko pona moli sama, sama sijelo ona jo. mod luka pakala ko, anu ko kute palisa.

ni ken seme suwi anpa, anu olin waso tu. waso kiwen linja oko n. noka kule akesi oko ni, pali utala o kin. taso lawa kiwen oth n, sike mama n oth, anu ni telo anpa.

