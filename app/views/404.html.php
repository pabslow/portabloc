<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes" />
	<title>404 Not Found | <?php echo config('blog.title') ?></title>
	<link href="<?php echo site_url() ?>assets/neu.css" rel="stylesheet" />
</head>
<body>
	<content>
	<h1>404 <span class="normal">: Not found</span></h1>
	<img src="<?php echo site_url(); ?>media/cat-in-a-box.png">
	<p><a href="<?php echo site_url(); ?>">&larr; Back to home</a></p>
	</content>
</body>
</html>

